package ws2811gw

// Copyright 2016 Jacques Supcik / Bluemasters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*
This package gives the required mechanisms to implement a web based server
for a ws2811 device (Neopixel). It uses "websockets" for close to real time
communication. It can be seen as a "gateway" between clients and the neopixels.
*/

import (
	log "github.com/sirupsen/logrus"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"net/http"
)

const (
	frameChannelLen = 2
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  4096,
	WriteBufferSize: 4096,
}

// Ws2811Engine is an interface for a driver based on the ws2811 chip.
// It can be a "real" ws2811 or a simulator
type Ws2811Engine interface {
	Init() error
	Render() error
	Wait() error
	Fini()
	SetLed(int, uint32)
}

// Ws2811Factory is a function that creates a Ws2811Engine based on the options
// given in the HTTP request.
type Ws2811Factory func(r *http.Request) Ws2811Engine

// Init starts a gorouting for handling requests and setup routing for the HTTP server
// r is a pointer to mux.Router from the Gorilla Toolkit (http://www.gorillatoolkit.org/pkg/mux)
// factory is a function that returns a ws2811 engine.
func Init(r *mux.Router, factory Ws2811Factory) {
	opener := make(chan *http.Request)
	closer := make(chan struct{})
	frames := make(chan *[]byte, frameChannelLen)

	var ws Ws2811Engine
	var connection *websocket.Conn

	go func() {
		for {
			select {
			case r := <-opener:
				if ws != nil {
					ws.Fini()
				}
				ws = factory(r)
				err := ws.Init()
				if err != nil {
					log.Fatal(err)
				}
			case <-closer:
				if ws != nil {
					ws.Fini()
					ws = nil
				}
			case f := <-frames:
				if ws != nil {
					for i := 0; i < len(*f)/3; i++ {
						ws.SetLed(i, uint32((*f)[i*3])<<16+uint32((*f)[i*3+1])<<8+uint32((*f)[i*3+2]))
					}
					err := ws.Wait()
					if err != nil {
						log.Fatal(err)
					}
					err = ws.Render()
					if err != nil {
						log.Fatal(err)
					}
				}
			}
		}
	}()

	r.HandleFunc("/open", func(w http.ResponseWriter, r *http.Request) {
		log.Debugf("Opening %v", r.URL.String())
		opener <- r

		var err error
		connection, err = upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Errorf("Upgrader error: %v", err)
			return
		}
		log.Debugln("Accepting frames")
		for {
			_, p, err := connection.ReadMessage()
			log.Debugln("Incoming frame")
			if err != nil {
				log.Debug("Bye")
				return
			}
			frames <- &p
			connection.WriteMessage(websocket.BinaryMessage, []byte{0})
		}
		log.Debugln("Exit from loop")

	})

	r.HandleFunc("/close", func(w http.ResponseWriter, r *http.Request) {
		log.Debugln("Closing")
		closer <- struct{}{}
		if connection != nil {
			connection.Close()
		}
	})

}
